package helena.maria.appx0e

import android.app.Activity
import android.content.Intent
import android.net.Uri
import  androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var storage : StorageReference
    lateinit var db : CollectionReference
    lateinit var alFile : ArrayList<HashMap<String,Any>>
    lateinit var adapter: CustomAdapter
    lateinit var uri : Uri
    val F_ID = "id"
    val F_NAME = "file_name"
    val F_TYPE = "file_type"
    val F_URL = "file_url"
    val RC_OK = 100
    var fileType =""
    var fileName =""
    var fileId =""
    var docName = ""
    var docId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnUpPdf.setOnClickListener(this)
        btnUpImg.setOnClickListener(this)
        btnUpVid.setOnClickListener(this)
        btnUpWord.setOnClickListener(this)
        btnUpload.setOnClickListener(this)
        lsV.setOnItemClickListener(itemClick)
        alFile = ArrayList()
        uri = Uri.EMPTY
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alFile.get(position)
        docId = hm.get(F_ID).toString()
        docName = hm.get(F_NAME).toString()
        var extensi = hm.get(F_TYPE).toString()
        var pesan = "file : $docName$extensi  Hapus Data!"
        txSelectedFile.setText(pesan)
    }

    override fun onStart() {
        super.onStart()

        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("files")
        db.addSnapshotListener { querySnapshot, firebaseFireStoreException ->
            if (firebaseFireStoreException!=null){
                firebaseFireStoreException.message?.let{Log.e("Firestore : ",it)}
            }
            showData()
        }
    }
    fun showData(){
        db.get().addOnSuccessListener { result ->
            alFile.clear()
            for (doc in result){
                val hm = HashMap<String,Any>()
                hm.put(F_ID, doc.get(F_ID).toString())
                hm.put(F_TYPE, doc.get(F_TYPE).toString())
                hm.put(F_NAME, doc.get(F_NAME).toString())
                hm.put(F_URL, doc.get(F_URL).toString())
                alFile.add(hm)
            }
            adapter = CustomAdapter(this,alFile)
            lsV.adapter = adapter
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if ((resultCode == Activity.RESULT_OK) && (requestCode == RC_OK)){
            if(data != null) {
                uri = data.data!!
                txSelectedFile.setText(uri.toString())
            }
        }
    }
    override fun onClick(v: View?) {
        val intent = Intent()
        intent.action = Intent.ACTION_GET_CONTENT
        when(v?.id){
            R.id.btnUpWord ->{
                fileType = ".docx"
                intent.setType(
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document")}
            R.id.btnUpVid ->{
                fileType = ".mp4"
                intent.setType("video/*")}
            R.id.btnUpImg ->{
                fileType = ".jpg"
                intent.setType("image/*")}
            R.id.btnUpPdf ->{
                fileType = ".pdf"
                intent.setType("application/pdf")}
            R.id.btnUpload ->{
                if (uri !=null){
                    fileName = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
                    val fileRef = storage.child(fileName+fileType)
                    fileRef.putFile(uri)
                        .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                            return@Continuation fileRef.downloadUrl
                        })
                        .addOnCompleteListener { taks ->
                            val  hm = HashMap<String,Any>()
                            hm.put(F_ID, fileName)
                            hm.put(F_NAME, fileName)
                            hm.put(F_TYPE,fileType)
                            hm.put(F_URL, taks.result.toString())
                            db.document(fileName).set(hm).addOnSuccessListener {
                                Toast.makeText(
                                    this,
                                    "File Telah Terupload",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                }
            }

        }
        if(v?.id == R.id.btnUpWord || v?.id == R.id.btnUpVid  || v?.id == R.id.btnUpImg  || v?.id == R.id.btnUpPdf ) startActivityForResult(intent,RC_OK) //untuk bisa membuka explorer di hp
    }

}